import time
import os
import sys

message_number = 1

while True:
    print(f"Hello. I'm writing messages! This message number is {message_number}.")
    message_number += 1
    sys.stdout.flush()  # Flush the output buffer
    time.sleep(2)