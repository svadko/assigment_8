# Use the official Python image as the base image
FROM python:3.9

WORKDIR /app

COPY python_script.py /app/

CMD ["python3","-u","python_script.py"]
