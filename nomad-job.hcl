variable "docker_image" {}

job "python_scripts" {
  datacenters = ["dc1"]

  group "python" {
    count = 2

    network {
      port "python" {}
    }
    task "python_task" {
      driver = "docker"

      config {
        image = var.docker_image
        ports = ["python"]
      }
    }
  }
}
